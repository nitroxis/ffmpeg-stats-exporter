use std::{
	future::ready,
	io::ErrorKind,
	sync::Arc,
	time::{Duration, Instant},
};

use axum::{
	extract::{BodyStream, Path},
	routing::{get, post},
	Extension, Router,
};
use clap::Parser;
use colored::*;
use futures_util::TryStreamExt;
use metrics::*;
use metrics_exporter_prometheus::PrometheusBuilder;
use metrics_util::MetricKindMask;
use regex::Regex;
use tokio::{io::AsyncBufReadExt, sync::Mutex, time::sleep};
use tokio_util::io::StreamReader;

const AVG_WINDOW: f64 = 2.0f64;

#[derive(Parser, Debug)]
struct Opts {
	/// Minimum FPS, all values below this will be printed red.
	#[arg(long)]
	min_fps: Option<f64>,

	/// Minimum speed, all values below this will be printed red.
	#[arg(long)]
	min_speed: Option<f64>,

	/// Print interval
	#[arg(short, long, default_value = "0.2")]
	interval: f64,

	/// HTTP listen end point.
	#[arg(short, long, default_value = "0.0.0.0:7878")]
	listen: String,
}

#[tokio::main]
async fn main() {
	let opts = Opts::parse();
	let opts = Arc::new(opts);

	let h = PrometheusBuilder::new()
		.idle_timeout(MetricKindMask::ALL, Some(Duration::from_secs(5)))
		.install_recorder()
		.unwrap();

	let app = Router::new()
		.route("/progress/:name", post(progress))
		.route("/metrics", get(move || ready(h.render())))
		.layer(Extension(opts.clone()));

	axum::Server::bind(&opts.listen.parse().unwrap())
		.serve(app.into_make_service())
		.await
		.unwrap();
}

async fn progress(opts: Extension<Arc<Opts>>, Path(name): Path<String>, body: BodyStream) {
	let body = body.map_err(|e| std::io::Error::new(ErrorKind::Other, e));
	let reader = StreamReader::new(body);
	let mut lines = reader.lines();

	let mut current_stats = Stats::default();
	let stats: Arc<Mutex<Vec<(Instant, Stats)>>> = Arc::new(Mutex::new(Vec::new()));

	let printer = async {
		let interval = Duration::from_secs_f64(opts.interval);

		loop {
			sleep(interval).await;

			let history = stats.lock().await;
			if history.len() < 1 {
				continue;
			}

			let (t1, v1) = history[history.len() - 1];
			let t0 = t1 - Duration::from_secs_f64(AVG_WINDOW);
			let v0 = lerp(history.as_slice(), t0).1;

			let fps = (v1.frame - v0.frame) / AVG_WINDOW;
			let speed = (v1.time - v0.time) / AVG_WINDOW;
			let bitrate = (v1.size - v0.size) / AVG_WINDOW / (1024f64 / 8f64);

			let text = format!("FPS: {fps:>7.2}, Speed: {speed:>6.3}, Bitrate: {bitrate:>9.2} kbits/s");

			let mut is_warn = false;

			if let Some(min) = opts.min_fps {
				if fps < min {
					is_warn = true;
				}
			}

			if let Some(min) = opts.min_speed {
				if speed < min {
					is_warn = true;
				}
			}

			if is_warn {
				println!("{}", text.red());
			} else {
				println!("{}", text);
			}
		}
	};

	let reader = async {
		let metric_frames = register_gauge!("ffmpeg_frames", "name" => name.clone());
		let metric_time = register_gauge!("ffmpeg_time", "name" => name.clone());
		let metric_size = register_gauge!("ffmpeg_size", "name" => name.clone());
		let stream_regex = Regex::new(r"^stream_(\d+)_(\d+)_(.+)$").unwrap();

		while let Ok(Some(line)) = lines.next_line().await {
			if let Some((k, v)) = line.split_once('=') {
				match k {
					"frame" => {
						let v: f64 = v.parse().unwrap_or_default();
						metric_frames.set(v);
						current_stats.frame = v;
					}
					"out_time_us" => {
						let v: f64 = v.parse().unwrap_or_default();
						if v >= 0.0 {
							let v = v / 1_000_000.0f64;
							metric_time.set(v);
							current_stats.time = v;
						}
					}
					"total_size" => {
						let v: f64 = v.parse().unwrap_or_default();
						metric_size.set(v);
						current_stats.size = v;
					}
					"progress" => {
						let mut lock = stats.lock().await;

						let cutoff = Instant::now() - Duration::from_secs_f64(AVG_WINDOW) - Duration::from_secs(1);

						while lock.len() > 0 && lock[0].0 < cutoff {
							lock.remove(0);
						}

						lock.push((Instant::now(), current_stats));
					}
					_ => {
						if let Some(cap) = stream_regex.captures(k) {
							let v: f64 = v.parse().unwrap_or_default();
							let file = cap[1].to_string();
							let stream = cap[2].to_string();
							let key = cap[3].to_string();
							gauge!("ffmpeg_stream", v, "name" => name.clone(), "file" => file, "stream" => stream, "key" => key);
						}
					}
				}
			}
		}
	};

	tokio::select!(
		_ = printer => {},
		_ = reader => {}
	);
}

#[derive(Copy, Clone, Default)]
struct Stats {
	frame: f64,
	time: f64,
	size: f64,
}

fn lerp(history: &[(Instant, Stats)], t: Instant) -> (usize, Stats) {
	match history.binary_search_by(|(t2, _)| t2.cmp(&t)) {
		Ok(i) => (i, history[i].1),
		Err(i) => {
			if i == 0 {
				(0, history[0].1)
			} else if i == history.len() {
				(i - 1, history[history.len() - 1].1)
			} else {
				let (t0, v0) = history[i - 1];
				let (t1, v1) = history[i];

				let dt = t.duration_since(t0).as_secs_f64();
				let time_span = t1.duration_since(t0).as_secs_f64();
				let f = dt / time_span;
				let lerped = Stats {
					frame: v0.frame + (v1.frame - v0.frame) * f,
					time: v0.time + (v1.time - v0.time) * f,
					size: v0.size + (v1.size - v0.size) * f,
				};
				(i - 1, lerped)
			}
		}
	}
}
